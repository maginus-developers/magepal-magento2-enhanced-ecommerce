1.4.0
=============
* ##### New Features:
  * Add data layer API for product and category
  * Code refactoring
  * Add position to data layer
  * Other minor improvements

1.2.1
=============
* ##### New Features:
  * Add support for <a href="https://amasty.com/ajax-shopping-cart-for-magento-2.html?a=magepal" rel="nofollow" _blank="new">Amasty Ajax to Cart</a>
  * Add checkout events
    * checkoutShippingStepCompleted
    * checkoutShippingStepFailed
    * checkoutPaymentStepCompleted
    * checkoutPaymentStepFailed
    * shippingMethodAdded
    * paymentMethodAdded
    * checkoutEmailValidation
  * Add Javascript Trigger
    * mpCheckoutShippingStepValidation
    * mpCheckoutPaymentStepValidation

* ##### Fixed bugs:
  * Fix "class does not exist" when saving admin order
  * Update checkout steps logic to prevent duplicate events


1.2.0
=============
* ##### New Features:
  * Add product variant
  * Add product category (auto select first category)
  * Api to quickly add more content to the data layer (see base GTM for more information)
  * Add jQuery trigger event for mpCustomerSession, mpItemAddToCart, mpItemRemoveFromCart, mpCheckout and mpCheckoutOption

* ##### Fixed bugs:
  * Add 'currencyCode' to every page
  * Edit add to cart item on product detail page


1.1.6
=============
* ##### New Features:
  * Add support for Enhanced Success Page

* ##### Fixed bugs:
  * None


1.1.5
=============
* ##### New Features:
  * Add support for canceled order refund tracking
  * Add support for admin order tracking

* ##### Fixed bugs:
  * Fixed issue with admin Credit Memo Refund not reporting correctly


1.1.4
=============
* ##### New Features:
  * None

* ##### Fixed bugs:
  * Fixed file corruption in system.xml


1.1.3
=============
* ##### New Features:
  * None

* ##### Fixed bugs:
  * Fixed typo in system.xml
  * Add currency code to product data layer


1.1.2
=============
* ##### New Features:
  * Add support for 2.3.0

* ##### Fixed bugs:
  * None
  
  
1.1.1
=============  
* First release
